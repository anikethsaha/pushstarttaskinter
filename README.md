# PushStart Task
This is boilerplate for MERN stack with integrations like Redux and SSR


## Getting Started
##### Clone the project
##### Install Dependecies`npm i`
##### `npm run build:local` = run the webpack in watch mode
##### `npm run server:dev`= run the express server locally

> Please Change the Mongodb URL to your URL in config/index.js : MONGO_URL

##### To run the webpack-dev-server `npm run client:dev`

## Look for the `config/index.js` for making changes in the configs of the project
- Edit the src/client/index.js to make changes for client

- Edit the src/components to make changes in components

- Edit the src/server/index.js to make changes in the server



**Built With [MERN-BoilerPlate](https://github.com/anikethsaha/MERNG-BoilerPlate)**
> Checkout the MERN-Boilerpla Branch for MERN stack Development
