import { createStore, combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer } from './reducer'

const rootReducer = combineReducers({
  reducer,
  form: formReducer
})




export const store = createStore(rootReducer);


store.subscribe(async () =>
  console.log("> ACTION DISPATCHED <" , await store.getState())
)

