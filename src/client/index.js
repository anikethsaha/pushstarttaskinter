import  App  from "../common/components/app.jsx"
import React from "react"
import ReactDOM from 'react-dom'

ReactDOM.hydrate(
      <App />,
    document.getElementById('app')
)
