import React from 'react';
import MyCard from './myCard.jsx';
import { Doughnut } from 'react-chartjs-2';
class MidOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      donutOneData : {
        datasets: [{
          data: [300, 50, 100],
          backgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56'
          ],
          hoverBackgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56'
          ]
        }]
      },
      donutTwoData : {
        datasets: [{
          data: [300, 50, 100],
          backgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56'
          ],
          hoverBackgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56'
          ]
        }]
      },
      donutOptions : {
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 2,
        percentageInnerCutout : 50,
        animationSteps : 100,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        responsive: true,
        maintainAspectRatio: true,
        showScale: true,
      }
    };
  }

  render() {
    return (
      <div>
        <MyCard title="Data Quality Index">
            <Doughnut
              data={this.state.donutOneData}
              width={100}
              height={50}
              options={this.state.donutOptions}
            />
        </MyCard>
        <MyCard title="Business Index">
            <Doughnut
              data={this.state.donutOneData}
              width={100}
              height={50}
              options={this.state.donutOptions}
            />
        </MyCard>

      </div>
    );
  }
}

MidOne.propTypes = {};

export default MidOne;
