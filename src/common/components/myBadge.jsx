import React from 'react';

class Badge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {

    return (
      <span className="badge rect" style={{"backgroundColor":this.props.color}}>
          {this.props.children}
      </span>
    );
  }
}

Badge.propTypes = {};

export default Badge;
