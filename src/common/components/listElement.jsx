import React from 'react';
import Badge from './myBadge.jsx';
class ListElement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <li className="row le">
        <div className="ten columns">
        <i class={this.props.iconClass || ""} aria-hidden="true"></i>
        <span className="spancntnt">
          <span className="">{ this.props.title }</span>
          <span className="">
            {
              this.props.badgeContent?
                <Badge color={this.props.badgeColor}>
                  { this.props.badgeContent}
                </Badge>
                : ""
          }
          </span>
        </span>
        <br/>
        <span className="meta-span">{this.props.metaTitle}</span>
        </div>
        <div className="two columns">
          <span style={{
            "color" :this.props.badgeColor || "inherit",
            "fontSize" : "1.2em"
          }}>{this.props.rightContent || ""}</span>
        </div>
      </li>
    );
  }
}

ListElement.propTypes = {};

export default ListElement;
