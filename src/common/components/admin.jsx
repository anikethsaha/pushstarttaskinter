import React from 'react';
import { Field, reduxForm } from 'redux-form'
import axios from 'axios'
import { connect } from "react-redux";
import { addTask } from '../../client/redux/actions'
import Button from '@atlaskit/button';
class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitStatus : "",
      flags : [],
      isFlag : false
    };
    this.dismissFlag = this.dismissFlag.bind(this);
    this.handlePostRequest = this.handlePostRequest.bind(this);
  }

  dismissFlag(){
    this.setState({ flags: [] });
  }
  handlePostRequest(values){

      axios.post('/task/add',values)
      .then(res => {
        const {data} = res;
        if(data.status == "SUCCESS"){
          this.setState({
            submitStatus : "SUCCESSFULLY ADDED"
          })
        }
        else{
          this.setState({
            submitStatus : "FAILED TO  ADD"
          })
        }
      })
      .catch(err => { // eslint-disable-line
        this.setState({
          submitStatus : "NETWORK/ARCHITECH PROBLEM",
        })
      })
  }
  render() {
    return (
      <section className="container">

        <div className="center">






          <div style={{
            width:"30%",
            margin :"auto"
          }}>
          <h3>Task Addition Form </h3>
          <span className="small-title">{this.state.submitStatus}</span>
            <form onSubmit={this.props.handleSubmit(this.handlePostRequest)} className="w-full" action="" method="post">
              <label >Title</label>
              <Field
                  name="taskTitle"
                  id="Title"
                  component="input"
                  type="text"
                />

              <label >Meta</label>
                <Field
                  name="taskMeta"
                  id="meta"
                  component="input"
                  type="text"
                />




              <label >Title</label>
              <Field name="taskLevel" component="select">
                <option value="">PLEASE SELECT LEVEL</option>
                <option value="LOW">LOW</option>
                <option value="MEDIUM">MEDIUM</option>
                <option value="HIGH">HIGH</option>
              </Field>




              <div>
              <Button
                  type="submit"
                  appearance="default"
                  className="w-full m-auto"
                >
                  Submit
                </Button>

              </div>
            </form>
          </div>
        </div>
      </section>
    );
  }
}

export default  connect(
  null,
  {addTask}
)(
  reduxForm(
    {
      form: 'task'
    }
  )(Admin)
)

 ;
