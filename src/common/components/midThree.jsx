import React from 'react';
import AssignedToMe from './assignedToMe.jsx'
import MyCard from './myCard.jsx'
import ListElement from './listElement.jsx'
import Page, { Grid } from '@atlaskit/page';
class MidThree extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <AssignedToMe/>


        <MyCard title="Activity Stream">
            <Page>
              <Grid>
                  <ul className="m-list">
                      <ListElement
                      badgeColor="grey"
                      title="Workflow-Marketing data 1"
                      metaTitle="Finished running and 1000 errors"

                      rightContent="Today"
                      iconClass="fas fa-sitemap"
                       />
                       <ListElement
                      badgeColor="grey"
                      title="Data Library - earthquake.csv added"
                      metaTitle="earthquake csv added and updated"

                      rightContent="Today"
                      iconClass="fa fa-database"
                       />
                      <ListElement
                      badgeColor="grey"
                      title="@deleeps tagged you in a comment "
                      metaTitle="Have a look in my dataset !!!"

                      rightContent="Today"
                      iconClass="fa fa-tasks"
                       />
                      <ListElement
                        badgeColor="grey"
                        title="Error - 10000 errors detected"
                        metaTitle="New error detected"

                        rightContent="Today"
                        iconClass="fas fa-bug"
                       />
                       <ListElement
                        badgeColor="grey"
                        title="Job - Marketing data 1 success"
                        metaTitle="New error detected"

                        rightContent="Today"
                        iconClass="fa fa-play"
                       />
                  </ul>
              </Grid>
            </Page>
        </MyCard>




      </div>
    );
  }
}

MidThree.propTypes = {};

export default MidThree;
