import React from 'react';
import MyCard from './myCard.jsx';
import Page, { Grid } from '@atlaskit/page';
import ListElement from './listElement.jsx';
// import _ from "lodash";
import { connect } from 'react-redux';
import axios from "axios"
import {addTask} from "../../client/redux/actions"
class AssignedToMe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isTaskLoaded : false
    };
  }
  componentWillMount(){
    axios.get('/task/get/all')
    .then(res => {
      const {data} = res;
      if(data.status == "FAILED"){
        // NETWORK FAILED ERROR IN FLAG
      }else{
        this.props.addTask(data.task)
        // this.setState()
        this.setState({
          isTaskLoaded : true
        })

      }
    })
    // eslint-disable-next-line no-unused-vars
    .catch(err =>{
      // NETWORK FAILED ERROR IN FLAG
    })
  }



  render() {

    return (
      <MyCard title="Top Errors">
      <Page>
        <Grid>
            <ul className="m-list">
            {
              this.state.isTaskLoaded? this.props.task[0].map((_task) => {
              return (
                    <ListElement
                      badgeColor={
                        _task.taskLevel == "HIGH"? "red":_task.taskLevel == "MEDIUM"? "orange" : "green"
                      }
                      title={_task.taskTitle}
                      metaTitle={_task.taskMeta}
                      badgeContent={_task.taskLevel}

                       />
            )
          }) : "No Task Pending"
            }
            </ul>
        </Grid>
      </Page>
  </MyCard>
    );
  }
}
const mapStateToProps = state => {
  return {
    ...state.reducer
  }
}


export default connect(
  mapStateToProps,
  {addTask}
)(AssignedToMe);
