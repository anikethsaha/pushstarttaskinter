import React from 'react';
import { withRouter } from 'react-router-dom';


class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <header className="header">
      <div className="stuff">
          <div className="left">
            <div
              class="logo"
              onClick={() => {
                this.props.history.push("/");
              }}
            >
                Logo
            </div>
            <ul className="menu-left">
              <li>
                <a href="#">
                <i class="fa fa-database" aria-hidden="true"></i>
                  Data Library
                </a>
              </li>
              <li>
                <a href="#"><i class="fas fa-sitemap"></i>WorkFlows
                </a>
              </li>
              <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>Scheduler</a></li>
              <li><a href="#"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Error Message</a></li>
              <li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a></li>
              <li><a href="#"><i class="fa fa-file" aria-hidden="true"></i>Reports</a></li>

            </ul>
            <ul className="menu-right">
              <li><a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i></a></li>
              <li>
                <a
                onClick={()=>{
                this.props.history.push("/admin")
                }}
                >
                  <i class="fa fa-user" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </div>

      </div>

      </header>
    );
  }
}

Header.propTypes = {};

export default withRouter(Header);
