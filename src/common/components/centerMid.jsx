import React from 'react';
import MidOne from './midOne.jsx';
import MidThree from './midThree.jsx';
import MidTwo from './midTwo.jsx';

class Mid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="container">
          <div className="row">
              <div className="four columns">
                <MidOne />
              </div>
              <div className="four columns">
                <MidTwo />
              </div>
              <div className="four columns">
                  <MidThree />
              </div>
          </div>
      </div>
    );
  }
}

Mid.propTypes = {};

export default Mid;
