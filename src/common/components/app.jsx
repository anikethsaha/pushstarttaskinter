import React from "react";

// var React = require('react');
import {Provider} from 'react-redux'
import { store } from "../../client/redux";
import Header from "./header.jsx";
import Main from './main.jsx'
import Admin from './admin.jsx'
import {
  Switch,
  HashRouter ,
  Route,
} from 'react-router-dom'

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    render() {
        return (

              <Provider store = {store}>

                <HashRouter>
                <div>
                    <Header />
                    <div className="body">

                      <Switch>
                        <Route exact path="/" component={Main} />
                        <Route exact path="/admin" component={Admin} />
                      </Switch>

                    </div>
                </div>
                  </HashRouter>

               </Provider>

        );
    }
}

