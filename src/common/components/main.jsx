import React from 'react'
import Center from "./centerGraph.jsx"
import Mid from "./centerMid.jsx"
class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Center />
        <Mid />
      </div>
    );
  }
}

Main.propTypes = {};

export default Main;
