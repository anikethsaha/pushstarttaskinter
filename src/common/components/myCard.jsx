import React from 'react';

import Page, { Grid } from '@atlaskit/page';
class MyCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div style={{
        "marginTop": ".8em"
      }}>
        <span className="small-title m1">{ this.props.title }</span>
            <Page>
              <Grid>
                <div className="card m0 p1 w-full soft-corner">
                    { this.props.children }
                </div>

              </Grid>
            </Page>
      </div>
    );
  }
}

MyCard.propTypes = {};

export default MyCard;
