import React from 'react';
import MyCard from './myCard.jsx';
import Page, { Grid } from '@atlaskit/page';
import ListElement from './listElement.jsx';

class MidTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <MyCard title="Top Errors">
            <Page>
              <Grid>
                  <ul className="m-list">
                      <ListElement
                      badgeColor="red"
                      title="Premium Less Than Zero"
                      metaTitle="MARKETING WORKFLOW 1"
                      badgeContent="HIGH"
                      rightContent="1500"
                       />
                       <ListElement
                      badgeColor="red"
                      title="Sum Insured Less Than Zero"
                      metaTitle="MARKETING WORKFLOW 1"
                      badgeContent="HIGH"
                      rightContent="1500"
                       />
                        <ListElement
                      badgeColor="orange"
                      title="Incorrect City Name &amp; Addess"
                      metaTitle="MARKETING WORKFLOW 1"
                      badgeContent="MEDIUM"
                      rightContent="1500"
                       />
                  </ul>
              </Grid>
            </Page>
        </MyCard>

        <MyCard title="Highest Business Impact">
            <Page>
              <Grid>
                  <ul className="m-list">
                      <ListElement
                      badgeColor="red"
                      title="Premium Less Than Zero"
                      metaTitle="MARKETING WORKFLOW 1"
                      badgeContent="HIGH"
                      rightContent="1500"
                       />
                       <ListElement
                      badgeColor="red"
                      title="Sum Insured Less Than Zero"
                      metaTitle="MARKETING WORKFLOW 1"
                      badgeContent="HIGH"
                      rightContent="1500"
                       />
                        <ListElement
                      badgeColor="orange"
                      title="Incorrect City Name &amp; Addess"
                      metaTitle="MARKETING WORKFLOW 1"
                      badgeContent="MEDIUM"
                      rightContent="1500"
                       />
                  </ul>
              </Grid>
            </Page>
        </MyCard>



      </div>
    );
  }
}

MidTwo.propTypes = {};

export default MidTwo;
