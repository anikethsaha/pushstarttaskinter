'use strict'

import mongoose from 'mongoose'
const Schema = mongoose.Schema

let TaskSchema = new Schema({
  _id : {
    type : Schema.Types.ObjectId,
    auto : true,
    required : true
  },
  taskTitle : {
      type :String,
      required:true
  },
  taskMeta : {
      type :String,
      required : true
  },
  taskLevel : {
      type : String,
      required : true
  }
},{
  timestamps : true
});

export default mongoose.model('Task', TaskSchema)
