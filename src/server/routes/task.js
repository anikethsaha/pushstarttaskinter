const router = require('express').Router();
import {
  addTask,
  getAllTask
} from '../controller/task'


router.post('/add',addTask)
router.get('/get/all',getAllTask)

export default router;
