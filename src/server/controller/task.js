
import taskModel from '../model/task'

export const addTask = (req,res) => {
  const {
    taskTitle,
    taskMeta,
    taskLevel
  } = req.body
  return new taskModel({
    taskLevel,
    taskMeta,
    taskTitle
  }).save((err,newTask) => {
    if(err){
      res.json({
        status : "FAILED",
        err
      })
    }else{
      res.json({
        status :"SUCCESS",
        newTask
      })
    }
  })

}

export const getAllTask = (_req,res) => {
  taskModel.find({},(err,allTask)=>{
    if (err){
      res.json({
        status : "FAILED",
        message: "NETWORK ERROR",
        err
      })
    }
    res.json({
      status  : "SUCCESS",
      task : allTask

    })
  })
}
